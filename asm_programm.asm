extern printf

SECTION .data
    positive_sum_outout:	      db "Positive summa = %d", 10, 0
    negative_sum_output:	      db "Negative summa = %d", 10, 0
    ; Сообщение, если есть единица в 7 бите
    seven_bit_output:             db "Seven bit is set.", 10, 0 
    array                         dw 1, 2, 3, 4, 5, 80, 20, 0, 0, -10064, 20, 11
    ; array                         dw 10064 10000
    number_elements:              dd  24


SECTION .bss
    positive_sum: resb 2            ; 2 байта зарезирвировано для положительных чисел
    negative_sum: resb 2            ; 2 байта зарезирвировано для отрицательных чисел
	
SECTION .text
GLOBAL	main
main
    mov ebx, array                  ; адрес начала массива (поместить в регистр ebx адрес начала массива
    mov ecx, 0                      ; поместить в регистр ecx значение 0
    start_for:                      ; метка для цикла 
        mov eax, 0
        cmp ecx, [number_elements]  ; условия для работы цикла (команда для сравнения) 
        je end_for                  ; если ecx == 24, то цикл заканчивается

        mov eax, [ebx+ecx]          ;ecx смещение
        and rax, 0x000000000000ffff ; конъюнкция для удаления лишних битов, маска для удаления лишних битов в старших разрядах

        push ax                      ;поместить значения в стек
        shl ax,1                     ;для определения отрицательного числа (логический сдвиг влево
                                    ; при логическом сдвиге "освобождающиеся" биты заполняются нулями
                                    ;последний ушедший бит сохраняется во флаге CF
        pop ax                       ;извлечение из стека значений
        jc label_negative           ; проверяет флаговый регистр CF (станет 1, если старший бит равен 1 (число отрицательно)

        cmp rax, 0                  ; проверяет больше или равно 0;
        jge label_positive          ; метка для перехода, если больше или равно 0

        label_negative:             ;метка
            add r15, rax            ;сложение двух чисел, сумма помещается в r15
            add ecx, 2              ; чтобы получить следующий элемент массива
            jmp start_for

        label_positive:          
            add r14, rax
            add ecx, 2
            jmp start_for

    end_for:
        mov [positive_sum], r14    ; поместить значение r14 в positive_sum
        mov	rdi, positive_sum_outout    ; значение для первого аргумента printf

        mov	rsi, [positive_sum]          ;значение для второго аргумента printf
        mov	rax, 0
        call	printf    ;Вызов функции printf

        ; (131042 % 65536) - 65536
        mov [negative_sum], r15       
        mov rax, [negative_sum]
        mov rbx, 65536              ;делитель
        div rbx                   ;взять значение из rax и разделить на rbx
        cmp rdx, 0                ;
        je zero_label                   ; переход, если rdx равен 0
        sub rdx, 65536                ; разность, результат помещается в rdx
        mov [negative_sum], rdx         ; для красивого выхода

    zero_label:
        mov	rdi, negative_sum_output
        mov	rsi, [negative_sum]
        mov	rax, 0
        call	printf

    mov rdx, [negative_sum]
    and rdx, 0x000000000000ffff ; for delete extra bits     

    bt rdx, 6                    ;проверка 7 бита, сохраняет значение одного бита во флаге CF
    jnc label_seven_bit         ; если 7 бит установлен, то выйти из программы
    mov rdi, seven_bit_output
    mov rax, 0
    call printf

    label_seven_bit: 
        mov     rax, 60             ; выход
        xor     rdi, rdi
        syscall   