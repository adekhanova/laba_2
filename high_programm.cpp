#include <iostream>

using namespace std;

int main(){
    short array[] = {0, 0, 0, 0, 0, -80, -20, 0, 0, 69, 20, 11};
    int array_length = sizeof(array) / 2;
    short positive_sum = 0;
    short negative_sum = 0;
    for (int i = 0; i < array_length; i++){
        bool is_positive_number = array[i] > 0;
        if (is_positive_number)
        {
            positive_sum += array[i];
            continue;
        }
        
        bool is_negative_number = array[i] < 0;
        if (is_negative_number)
        {
            negative_sum += array[i];
        }        
    }
    cout << "Positive summa = " << positive_sum << endl;
    cout << "Negative summa = " << negative_sum << endl;
    
    int index_bit = 6;
    short negative_seven_bit_value = (negative_sum >> index_bit) & 1u; //побитовый сдвиг вправо 
    cout << 1u << endl;
    cout << "Seven bit value = " << negative_seven_bit_value << endl;
    
    
    // for(int i=0; i<8;i++)
    //     cout<<(positive_sum & (256>>i + 1)?'1':'0');
    //  cout<<endl;
    return 0;
}