extern printf

SECTION .data
    hello_world_output:	      db "Hello world", 10, 0

SECTION .text
GLOBAL	main
main
    mov	rdi, hello_world_output    ; значение для первого аргумента printf
    mov	rax, 0
    call	printf    ;Вызов функции printf

    mov     rax, 60             ; выход
    xor     rdi, 0
    syscall   
